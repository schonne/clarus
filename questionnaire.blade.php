<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <style>
        body {
            margin:0;
            padding: 0;
        }
        header,
        footer {
            display: none;
        }

        .step,
        .info,
        .total {
            padding: 20px;
            margin-top: 20px;
            margin-bottom: 20px;
            border-radius: 5px;
            font-size: 16px;
        }

        .info > span,
        .total > span {
            float: right;
        }

        .step {
            border: 2px solid #1a2d51;
        }

        .info {
            background: #f1f2f5;
        }

        .margin-bottom {
            margin-bottom: 80px;
        }

        .check {
            padding-top: 350px;
        }

        .total {
            background: #4575c4;
            color: #fff;
        }

        .title-page strong,
        .total>strong {
            color: #fff;
        }

        .container {
            width: 640px;
            margin: 0 auto;
        }

        ol {
            list-style: none;
        }

        * {
            font-family: "sofia-pro", sans-serif;
        }

        strong {
            color: #4575c4;
        }

        .title-page {
            height: 900px;
            width: 850px;
            background-color: #4575c4;
            text-align: center;
            margin: 0 auto;
            padding-top:300px;
            color:#fff;
        }

        .title-page h1 {
            margin:auto;
            font-size: 32px;
        }

        .company {
            padding: 12px 100px;
            background: #3960a3;
            border-radius: 15px;
            display: inline-block;
            margin: 0 auto;
            /*                margin-bottom: 50px;
        *//*                margin-left: 150px;
        */                font-size: 24px;
            width: 50%;
        }
        span {
            color: #4575c4;
            font-weight: bold;
            text-align: right;
        }
        .summary {
            background-color: #4577c5;
            border-radius: 5px;
            text-align: center;
            color: #fff;
            padding:10px;
            margin-bottom: 20px;
        }
        .summary > p {
            font-size: 60px;
            margin: 10px auto;
        }
        .subtotal {
            width: 50%;
            float: left;
            padding:10px;
            box-sizing: border-box;
        }
        .subtotal > div {
            background: #fff;
            border-radius: 10px;
            color: #4577c5;
            padding:10px;
            margin-top: 10px;
        }
        .clearfix:after {
            content: "";
            display: table;
            clear: both;
        }

        h1 > strong {
            color: #7eb26e;
        }

        .info > p {
            display: inline-block;
            width: 400px;
            vertical-align: middle;
        }
        .info > p + span {
            padding-top: 16px;
        }
        hr {
            margin: 20px 0;
            background-color: #fff;
            border-top: 2px dashed #001937;
        }

        .title-page hr {
            background-color: #4575c4;
        }

        section {
            background-color: #4575c4;
            width: 100%;
            display: block;
            padding: 15%;
            text-align: center;
        }

        section h1 strong {
            color: white;
        }

        .pagebreak { page-break-before: always; }
    </style>
</head>
<body>
<div class="title-page">
    <div class="container">
        <div style="margin-bottom: 40px;">
            <h1>R&D Credit, Qualifying Report</h1>
            <br/><br/><br/>
            <strong>PREPARED BY CLARUS R<span style="font-size: 23px">&#43;</span>D</strong>

        </div>
        <div class="company">{{$company->name}}<br>{{$project->name}}</div>
        <div class="check" >
            <i style="font-style: normal;" id="date">{{$date}}</i>
            <hr>
            1233 Dublin, Columbus, OH 43215&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            614.545.9100
        </div>
    </div>
</div>
<div class="pagebreak"> </div>
<div class="container">
    <ol>
        <li>
            <h1><strong>Section 1:</strong> Company Information</h1>
            <div class="info">
                Company Name
                <span>{{$c_questions[204]['answer']}}</span>
            </div>
            <div class="info">
                What state do you operate in?
                <span>{{$c_questions[205]['answer']}}</span>
            </div>
            <div class="info">
                Your Name:
                <span>{{$c_questions[206]['answer']}}</span>
            </div>
            <div class="info">
                Your Title:
                <span>{{$c_questions[207]['answer']}}</span>
            </div>
            <div>
                Please provide a short description of the company
                <p><strong><i>{{$c_questions[208]['answer']}}</i></strong></p>
            </div>
        </li>
        <hr>
        <div class="pagebreak"> </div>
        <li>
            <h1><strong>Section 2:</strong> Qualified Startup</h1>
            <div class="info">
                In what year was the company founded?
                <span>{{$c_questions[197]['answer']}}</span>
            </div>
            <div class="info">
                In what year was the first gross revenue earned?
                <span>{{$c_questions[198]['answer']}}</span>
            </div>
            <br/>
            <div>
                Please provide your revenue for each of the last 5 years:
            </div>

            <div class="info">
                {{$company_project->year - 1}} Revenue:
                <span>{{'$' . number_format(empty($c_questions[199]['answer']) ? 0 : $c_questions[199]['answer'], 2, ".", "," )}}</span>
            </div>
            <div class="info">
                {{$company_project->year - 2}} Revenue:
                <span>{{'$' . number_format(empty($c_questions[200]['answer']) ? 0 : $c_questions[200]['answer'], 2, ".", "," )}}</span>
            </div>
            <div class="info">
                {{$company_project->year - 3}} Revenue:
                <span>{{'$' . number_format(empty($c_questions[201]['answer']) ? 0 : $c_questions[201]['answer'], 2, ".", "," )}}</span>
            </div>
            <div class="info">
                {{$company_project->year - 4}} Revenue:
                <span>{{'$' . number_format(empty($c_questions[202]['answer']) ? 0 : $c_questions[202]['answer'], 2, ".", "," )}}</span>
            </div>
            <div class="info">
                {{$company_project->year - 5}} Revenue:
                <span>{{'$' . number_format(empty($c_questions[203]['answer']) ? 0 : $c_questions[203]['answer'], 2, ".", "," )}}</span>
            </div>
        </li>
        <hr>
        {{-- This is where you want to loop through each component/project --}}

        <div class="pagebreak"> </div>
        <li>
            <section class="project-definition">
                <h1><strong>Section 3:</strong> {{$project->name}}</h1>
            </section>
            <h1><strong>Section 3.1:</strong> Business Component Definition</h1>
            <div>
                Please provide a short description of the innovative business component:
                <p><strong><i>{{$p_questions[158]['answer']}}</i></strong></p>
            </div>
            <div>
                Provide some background on why the initiative was undertaken, the customer problem, market
                opportunity, and what made is a unique opportunity for innovation:
                <p><strong><i>{{$p_questions[159]['answer']}}</i></strong></p>
            </div>
            <div>
                Please describe any significant improvements made during the tax year for which you are computing the credit:
                <p><strong><i>{{$p_questions[160]['answer']}}</i></strong></p>
            </div>
            <div>
                Was the innovation a product, process, technique, invention, formula, or computer software?  If so, what was it?
                <p><strong><i>{{$p_questions[161]['answer']}}</i></strong></p>
            </div>
            <div>
                Did you intend to hold it for sale, lease, or license?
                <p><strong><i>{{$p_questions[162]['answer']}}</i></strong></p>
            </div>
            <div>
                Provide a brief overview of the principle people and special skill sets involved in the project:
                <p><strong><i>{{$p_questions[163]['answer']}}</i></strong></p>
            </div>
        </li>
        <hr>
        <div class="pagebreak"> </div>
        <li>
            <h1><strong>Section 3.2:</strong> Excluded Activities</h1>
            <div>
                Did the activity involve any of the following excluded activities:
            </div>
            <div class="info">
                <p>Activities outside the US, Puerto Rico, or US territories?</p>
                <span>{{$p_questions[164]['answer']}}</span>
            </div>
            <div class="info">
                <p>Adaptation of an existing business component?</p>
                <span>{{$p_questions[166]['answer']}}</span>
            </div>
            <div class="info">
                <p>Duplication or reverse engineering of an existing business component?</p>
                <span>{{$p_questions[166]['answer']}}</span>
            </div>
            <div class="info">
                <p>Social Sciences - "soft sciences", arts or humanities?</p>
                <span>{{$p_questions[170]['answer']}}</span>
            </div>
            <div class="info">
                <p>Studies related to efficiencies, management operations or profitability, style, taste, cosmetic or seasonal design factors?</p>
                <span>{{$p_questions[172]['answer']}}</span>
            </div>
            <div class="info">
                Routine data collection?
                <span>{{$p_questions[174]['answer']}}</span>
            </div>
            <div class="info">
                Routine or ordinary QA testing?
                <span>{{$p_questions[176]['answer']}}</span>
            </div>
            @if($p_questions[178]['answer'] == 'Yes')
                <br>
            @endif
            <div class="info">
                Funded research
                <span>{{$p_questions[178]['answer']}}</span>
            </div>
            <div class="info">
                Did you receive any grants that helped to fund your innovation?
                <span>{{$p_questions[180]['answer']}}</span>
            </div>
        </li>
        <hr>
        <div class="pagebreak"> </div>
        <li>
            <h1><strong>Section 4.1:</strong> Permitted Purpose</h1>
            <div>
                Was it something entirely new or did it improve existing functionality, performance, reliability, or quality? What did it improve?
                <p><strong><i>{{$p_questions[183]['answer']}}</i></strong></p>
            </div>
        </li>
        <hr>
        <li>
            <h1><strong>Section 4.2:</strong> Technological in Nature</h1>
            <div>
                Did the innovation rely on existing principles for physical or biological science, engineering, or computer science? If so, which ones?
                <p><strong><i>{{$p_questions[184]['answer']}}</i></strong></p>
            </div>
        </li>
        <hr>
        <div class="pagebreak"> </div>
        <li>
            <h1><strong>Section 4.3:</strong> Elimination of Uncertainty</h1>
            <div class="info">
                <p>Was there uncertainty around your capability of developing or improving your innovation?</p>
                <span>{{$p_questions[185]['answer']}}</span>
            </div>
            <div class="info">
                <p>Was there uncertainty around the methodology to be used?</p>
                <span>{{$p_questions[186]['answer']}}</span>
            </div>
            <div class="info">
                <p>Was there uncertainty around the appropriate design?</p>
                <span>{{$p_questions[187]['answer']}}</span>
            </div>
            <div>
                If there was uncertainty about capability, methodology, or appropriate design, why were you uncertain?
                <p><strong><i>{{$p_questions[188]['answer']}}</i></strong></p>
            </div>
            <div>
                What, if any, other options did you consider that were not ultimately taken?
                <p><strong><i>{{$p_questions[189]['answer']}}</i></strong></p>
            </div>
        </li>
        <hr>
        <li>
            <h1><strong>Section 4.4:</strong> Process of Experimentation</h1>
            <div>
                Was there more than one alternative evaluated before deciding on one design or methodology for accomplishing the objective?
                <p><strong><i>{{$p_questions[190]['answer']}}</i></strong></p>
            </div>
            <div>
                Describe the process that you used for deciding your approach to developing this innovation.
                <p><strong><i>{{$p_questions[191]['answer']}}</i></strong></p>
            </div>
            <div>
                What did you try that didn't work? Why did it fail?
                <p><strong><i>{{$p_questions[182]['answer']}}</i></strong></p>
            </div>
            <div>
                Was there a path that you took that you ultimately abandoned? Why?
                <p><strong><i>{{$p_questions[192]['answer']}}</i></strong></p>
            </div>
        </li>
    </ol>
</div>
</body>
</html>
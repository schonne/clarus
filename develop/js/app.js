document.addEventListener("DOMContentLoaded", function () {



	// -----------------------------------------------------------------------------------------------------
	// ---------------------------------------------  NEW CODE ----------------------------------------
	// -----------------------------------------------------------------------------------------------------

	// OPEN/CLOSE IMPORT/EXPORT EXPENSES PANEL
	if (document.querySelector('[js-hook="import-export"]')) {
		document.querySelector('[js-hook="import-export"]').addEventListener('click', (event) => {
			document.querySelector('[js-hook="page-background"]').classList.toggle("is-open");
		});
	}

	// ---------------------------------------  MORE MENU ON THE DASHBOARD -----------------------------------

	// MORE MENU - OPEN (used on users page too)
	if (document.querySelector('[js-hook="project-dots"]')) {
		Array.from(document.querySelectorAll('[js-hook="project-dots"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				let popup = event.target.nextElementSibling.querySelector('[js-hook="annotation-messages"]');
				if (!popup.classList.contains('is-showing')) {
					Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
						el.classList.remove("is-showing");
						el.classList.remove("is-resending");
					});
			}
			popup.classList.toggle("is-showing");
			});
		});
	}

	// MORE MENU - CLOSE (this also closes the Add Project bubble and on the users page)
	if (document.querySelector('[js-hook="annotations-close"]')) {
		Array.from(document.querySelectorAll('[js-hook="annotations-close"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
					el.classList.remove("is-showing");
					el.classList.remove("is-deleting");
					el.classList.remove("is-renaming");
				});
			});
		});
	}

	// MORE MENU - DELETE CONFIRM
	if (document.querySelector('[js-hook="delete-project"]')) {
		Array.from(document.querySelectorAll('[js-hook="delete-project"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				element.closest('[js-hook="annotation-messages"]').classList.add("is-deleting");
			});
		});
	}

	// MORE MENU - DELETE ACTUAL (works on users page too)
	if (document.querySelector('[js-hook="delete-choice"]')) {
		Array.from(document.querySelectorAll('[js-hook="delete-choice"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				event.target.closest('[js-hook="annotation-messages"]').classList.remove("is-deleting");
				event.target.closest('[js-hook="annotation-messages"]').classList.remove("is-showing");
				const projectNode = event.target.closest('tr');
				while(projectNode.firstChild) {
					projectNode.firstChild.remove()
				}
			});
		});
	}

	// MORE MENU - RENAME CHOICE SELECT
	if (document.querySelector('[js-hook="rename-project"]')) {
		Array.from(document.querySelectorAll('[js-hook="rename-project"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				element.closest('[js-hook="annotation-messages"]').classList.add("is-renaming");
				element.closest('[js-hook="annotation-messages"]').querySelector('[js-hook="project-rename"]').value = "";
				element.closest('[js-hook="annotation-messages"]').querySelector('[js-hook="project-rename"]').focus();
			});
		});
	}

	// MORE MENU - CANCEL RENAME (this also closes the Add Project bubble)
	if (document.querySelector('[js-hook="cancelrename-choice"]')) {
		Array.from(document.querySelectorAll('[js-hook="cancelrename-choice"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
					el.classList.remove("is-showing");
					el.classList.remove("is-deleting");
					el.classList.remove("is-renaming");
				});
			});
		});
	}

	// MORE MENU - CONFIRM RENAME OF PROJECT
	if (document.querySelector('[js-hook="rename-choice"]')) {
		Array.from(document.querySelectorAll('[js-hook="rename-choice"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				const projectName = event.target.closest('tr').querySelector('[js-hook="company-name"]');
				const newName = event.target.closest('[js-hook="rename-project-form"]').querySelector('[js-hook="project-rename"]').value;
				if (newName.length < 3) {
					event.target.closest('[js-hook="rename-project-form"]').querySelector('[js-hook="project-rename"]').classList.add("shake");
					event.target.closest('[js-hook="rename-project-form"]').querySelector('[js-hook="project-rename"]').focus();
					setTimeout(() => {
						Array.from(document.querySelectorAll('[js-hook="project-rename"]')).forEach(function(el) {
							el.classList.remove("shake");
						});
					}, 1000);
				} else {
					projectName.innerHTML = newName;
					// SAVE THE NAME TO THE DATABASE HERE USING THE VALUE IN THE VARIABLE newName
					Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
						el.classList.remove("is-showing");
						el.classList.remove("is-deleting");
						el.classList.remove("is-renaming");
					});
				}
			});
		});
	}

	// MORE MENU - CANCEL DELETE
	if (document.querySelector('[js-hook="cancel-choice"]')) {
		Array.from(document.querySelectorAll('[js-hook="cancel-choice"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
					el.classList.remove("is-showing");
					el.classList.remove("is-deleting");
					el.classList.remove("is-renaming");
				});
			});
		});
	}

	// ----------------------------------  ADD PROJECT BUTTON ON THE DASHBOARD --------------------------------

	// ADD PROJECT - OPEN
	if (document.querySelector('[js-hook="add-project-button"]')) {
		Array.from(document.querySelectorAll('[js-hook="add-project-button"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				let popup = event.currentTarget.nextElementSibling.querySelector('[js-hook="annotation-messages"]');
				popup.querySelector('[js-hook="project-add-name"]').value = "";
				if (!popup.classList.contains('is-showing')) {
					Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
						el.classList.remove("is-showing");
					});
			}
			popup.classList.toggle("is-showing");
			popup.querySelector('[js-hook="project-rename"]').focus();
			});
		});
	}

	// ADD PROJECT - ACTUALLY ADD
	if (document.querySelector('[js-hook="create-choice"]')) {
		Array.from(document.querySelectorAll('[js-hook="create-choice"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				const addProject = event.target.closest('[js-hook="add-project-form"]').querySelector('[js-hook="project-add-name"]').value;
				if (addProject.length < 3) {
					event.target.closest('[js-hook="add-project-form"]').querySelector('[js-hook="project-add-name"]').classList.add("shake");
					event.target.closest('[js-hook="add-project-form"]').querySelector('[js-hook="project-add-name"]').focus();
					setTimeout(() => {
						Array.from(document.querySelectorAll('[js-hook="project-add-name"]')).forEach(function(el) {
							el.classList.remove("shake");
						});
					}, 1000);
				} else {
					// SAVE THE NAME TO THE DATABASE HERE USING THE VALUE IN THE VARIABLE addProject THEN JUST REFRESH THE PAGE
					location.reload();
				}
			});
		});
	}

	// ---------------------------------------------  CRUD USERS   ----------------------------------------


	if (document.querySelector('[js-hook="add-user-button"]')) {
		document.querySelector('[js-hook="add-user-button"]').addEventListener('click', (event) => {
			if (document.querySelector('[js-hook="add-user-form"]').classList.contains("is-showing")) {
				const userName = document.querySelector('[js-hook="new-user-name"]').value
				const userEmail = document.querySelector('[js-hook="new-user-email"]').value
				const userPermission = document.querySelector('[js-hook="new-user-permission"]').value
				document.querySelector('[js-hook="new-user-email-error"]').classList.remove("is-showing")
				if (userName.length < 3) {
					document.querySelector('[js-hook="new-user-name"]').classList.add("shake");
					document.querySelector('[js-hook="new-user-name"]').focus();
					setTimeout(() => {
						document.querySelector('[js-hook="new-user-name"]').classList.remove("shake");
					}, 1000);
				} else {
					if(/\S+@\S+\.\S+/.test(userEmail)) {
						// SAVE TO DATABASE WITH userName, userEmail and userPermission VARIABLE VALUES THEN THE PAGE WILL REFESH TO REFLECT THE CHANGES
						// IF EMAIL ALREADY EXISTS USE THIS CONDITIONAL
						const databaseDuplicateEmail = 0;
						if (userEmail === databaseDuplicateEmail) {
							document.querySelector('[js-hook="new-user-email-error"]').classList.add("is-showing")
						} else {
							location.reload();
						}
					} else {
						document.querySelector('[js-hook="new-user-email"]').classList.add("shake");
						document.querySelector('[js-hook="new-user-email"]').focus();
						setTimeout(() => {
							document.querySelector('[js-hook="new-user-email"]').classList.remove("shake");
						}, 1000);
					}
				}
			} else {
				document.querySelector('[js-hook="add-user-form"]').classList.add("is-showing");
			}
		});
	}

		// MORE MENU - DELETE USER CONFIRM
		if (document.querySelector('[js-hook="delete-user"]')) {
			Array.from(document.querySelectorAll('[js-hook="delete-user"]')).forEach(function(element) {
				element.addEventListener('click', (event) => {
					element.closest('[js-hook="annotation-messages"]').classList.add("is-deleting");
				});
			});
		}

		
		// MORE MENU - RESEND INVITE
		if (document.querySelector('[js-hook="resend-invite"]')) {
			Array.from(document.querySelectorAll('[js-hook="resend-invite"]')).forEach(function(element) {
				element.addEventListener('click', (event) => {
					element.closest('[js-hook="annotation-messages"]').classList.add("is-resending");
				});
			});
		}

	// MORE MENU - EDIT USER
		if (document.querySelector('[js-hook="edit-user"]')) {
			Array.from(document.querySelectorAll('[js-hook="edit-user"]')).forEach(function(element) {
				element.addEventListener('click', (event) => {
					const projectNode = event.target.closest('tr');
					document.querySelector('[js-hook="new-user-name"]').value = projectNode.querySelector('td:nth-child(1)').innerText
					document.querySelector('[js-hook="new-user-email"]').value = projectNode.querySelector('td:nth-child(2)').innerText
					document.querySelector('[js-hook="new-user-role"]').value = projectNode.querySelector('td:nth-child(3)').innerText
					document.querySelector('[js-hook="new-user-permission"]').value = projectNode.querySelector('td:nth-child(4)').innerText
					
					document.querySelector('[js-hook="add-user-form"]').classList.add("is-showing");
					Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
						el.classList.remove("is-showing");
						el.classList.remove("is-deleting");
						el.classList.remove("is-renaming");
					});
				});
			});
		}


	// ---------------------------------------  PAYROLL / CONTRACTING TOGGLE OPEN/CLOSE   ----------------------------------------


	// CARET TOGGLE
	if (document.querySelector('[js-hook="toggle-details"]')) {
		Array.from(document.querySelectorAll('[js-hook="toggle-details"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				element.closest('[js-hook="resource"]').classList.toggle("is-open");
			});
		});
	}

	// SHOW ALL
	if (document.querySelector('[js-hook="show-all"]')) {
		Array.from(document.querySelectorAll('[js-hook="show-all"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				Array.from(document.querySelectorAll('[js-hook="resource"]')).forEach(function(el) {
					el.classList.add("is-open");
				});
				console.log(element)
				element.classList.remove("is-showing");
				element.nextSibling.classList.add("is-showing");
			});
		});
	}

	// HIDE ALL
	if (document.querySelector('[js-hook="hide-all"]')) {
		Array.from(document.querySelectorAll('[js-hook="hide-all"]')).forEach(function(element) {
			element.addEventListener('click', (event) => {
				Array.from(document.querySelectorAll('[js-hook="resource"]')).forEach(function(el) {
					el.classList.remove("is-open");
				});
				element.classList.remove("is-showing");
				element.previousSibling.classList.add("is-showing");
			});
		});
	}

	// -----------------------------------------------------------------------------------------------------
	// ---------------------------------------------  PREVIOUS CODE ----------------------------------------
	// -----------------------------------------------------------------------------------------------------

	// OPEN/CLOSE NOTIFICATIONS PANEL
	// if (document.querySelector('nav.navbar [js-hook="notifications-icon"]')) {
	// 	document.querySelector('nav.navbar [js-hook="notifications-icon"]').addEventListener('click', (event) => {
	// 		document.querySelector('[js-hook="notifications-panel"]').classList.toggle("is-open");
	// 	});
	// }
	// if (document.querySelector('[js-hook="close-panel"]')) {
	// 	document.querySelector('[js-hook="close-panel"]').addEventListener('click', (event) => {
	// 		document.querySelector('[js-hook="notifications-panel"]').classList.toggle("is-open");
	// 	});
	// }
	// if (document.querySelector('[js-hook="resolved-button"]') && document.querySelector('[js-hook="resolved-notifications"]')) {
	// 	document.querySelector('[js-hook="resolved-button"]').addEventListener('click', (event) => {
	// 		document.querySelector('[js-hook="resolved-notifications"]').classList.add("is-open");
	// 	});
	// }


	// // ANNOTATIONS - ADD COMMENT
	// if (document.querySelector('[js-hook="annotation-comment"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="annotation-comment"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			Array.from(document.querySelectorAll('input.message-write')).forEach(function(el) {
	// 				el.value = "";
	// 			});
	// 			event.target.closest('[js-hook="annotation-action-items"]').classList.add("is-hidden");
	// 			event.target.closest('[js-hook="annotation-action-items"]').nextElementSibling.classList.add("is-showing");
	// 			document.querySelector('.annotation.is-showing').setAttribute("style", "top:-" + (document.querySelector('.annotation.is-showing').offsetHeight + 10) + "px");
	// 		});
	// 	});
	// }
	// // ANNOTATIONS - CANCEL NEW COMMENT
	// if (document.querySelector('[js-hook="new-message-cancel"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="new-message-cancel"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="annotation-new-message"]').classList.remove("is-showing");
	// 			event.target.closest('[js-hook="annotation-new-message"]').previousElementSibling.classList.remove("is-hidden");
	// 			document.querySelector('.annotation.is-showing').setAttribute("style", "top:-" + (document.querySelector('.annotation.is-showing').offsetHeight + 10) + "px");
	// 		});
	// 	});
	// }

	// // ANNOTATIONS - POST NEW COMMENT
	// if (document.querySelector('[js-hook="new-message-post"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="new-message-post"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			let comment = event.target.closest('[js-hook="annotation-new-message"]').querySelector('[js-hook="new-message-comment"]').value;
	// 			if (comment !== "") {
	// 				let htmlString = "<div class='message'><div class='message-head'><div class='author'>You</div><div class='date'>1/5/19</div></div><div class='message-body'>" + comment + "</div></div>";
	// 				let target = event.target.closest('[js-hook="annotation-messages"]').firstElementChild
	// 				target.innerHTML = htmlString + target.innerHTML;
	// 				event.target.closest('[js-hook="annotation-new-message"]').classList.remove("is-showing");
	// 				event.target.closest('[js-hook="annotation-new-message"]').previousElementSibling.classList.remove("is-hidden");
	// 				document.querySelector('.annotation.is-showing').setAttribute("style", "top:-" + (document.querySelector('.annotation.is-showing').offsetHeight + 10) + "px");
	// 				if (event.target.closest('[js-hook="annotation-messages"]').classList.contains('is-new')) {
	// 					Array.from(document.querySelectorAll('[js-hook="annotation-messages"].is-new')).forEach(function(el) {
	// 						el.classList.remove("is-new");
	// 					});
	// 				}
	// 			}
	// 		});
	// 	});
	// }

	// // ANNOTATIONS - RESOLVE
	// if (document.querySelector('[js-hook="annotation-resolve"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="annotation-resolve"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="annotation-resolve-action"]').classList.add("is-hidden");
	// 			event.target.closest('[js-hook="annotation-resolve-action"]').nextElementSibling.classList.add("is-showing");
	// 			document.querySelector('.annotation.is-showing').setAttribute("style", "top:-" + (document.querySelector('.annotation.is-showing').offsetHeight + 10) + "px");
	// 		});
	// 	});
	// }

	// // ANNOTATIONS - UNRESOLVE
	// if (document.querySelector('[js-hook="annotation-unresolve"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="annotation-unresolve"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="annotation-unresolve-action"]').classList.remove("is-showing");
	// 			event.target.closest('[js-hook="annotation-unresolve-action"]').previousElementSibling.classList.remove("is-hidden");
	// 			document.querySelector('.annotation.is-showing').setAttribute("style", "top:-" + (document.querySelector('.annotation.is-showing').offsetHeight + 10) + "px");
	// 		});
	// 	});
	// }

	// // ANNOTATIONS - OPEN
	// if (document.querySelector('[js-hook="annotation-icon"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="annotation-icon"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
				
	// 			let popup = event.target.nextElementSibling;
	// 			if (!popup.classList.contains('is-showing')) {
	// 				Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
	// 					el.classList.remove("is-showing");
	// 				});
	// 				Array.from(document.querySelectorAll('.annotation-icon.is-showing')).forEach(function(el) {
	// 					el.classList.remove("is-showing");
	// 				});
	// 			}

	// 			if (event.target.nextElementSibling.firstElementChild.innerHTML === "") {
	// 				event.target.nextElementSibling.classList.add("is-new");
	// 			} else {
	// 				event.target.nextElementSibling.classList.remove("is-new");
	// 			}
	// 			if (event.target.parentElement.classList.contains("is-resolved")) {
	// 				console.log("asd")
	// 				event.target.nextElementSibling.querySelector('[js-hook="annotation-resolve-action"]').classList.add("is-hidden");
	// 				event.target.nextElementSibling.querySelector('[js-hook="annotation-resolve-action"]').nextElementSibling.classList.add("is-showing");
	// 			}
	// 			event.target.classList.toggle("is-showing");
	// 			popup.classList.toggle("is-showing");
	// 			popup.setAttribute("style", "top:-" + (event.target.nextElementSibling.offsetHeight + 10) + "px");
	// 		});
	// 	});
	// }

	// // ANNOTATIONS - CLOSE
	// if (document.querySelector('[js-hook="annotations-close"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="annotations-close"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			let popup = event.target.closest('[js-hook="annotation-messages"]');
	// 			if (popup.classList.contains('is-showing')) {
	// 				Array.from(document.querySelectorAll('.annotation.is-showing')).forEach(function(el) {
	// 					el.classList.remove("is-showing");
	// 				});
	// 				Array.from(document.querySelectorAll('.annotation-icon.is-showing')).forEach(function(el) {
	// 					el.classList.remove("is-showing");
	// 				});
	// 			}
	// 		});
	// 	});
	// }


	// // TOGGLE DESCRIPTION FOR RESOURCE TYPE ON RESOURCES
	// if (document.querySelector('.resources-page [js-hook="toggle-paragraph"]')) {
	// 	Array.from(document.querySelectorAll('.resources-page [js-hook="toggle-paragraph"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="description"]').classList.toggle("is-closed");
	// 		});
	// 	});
	// }

	// // TOGGLE DETAILS FOR RESOURCE ON RESOURCES
	// if (document.querySelector('.resources-page [js-hook="toggle-resource"]')) {
	// 	Array.from(document.querySelectorAll('.resources-page [js-hook="toggle-resource"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="resource"]').classList.toggle("is-open");
	// 		});
	// 	});
	// }

	// // ADD NEW EMPLOYEE
	// if (document.querySelector('.resources-page [js-hook="add-new-employee"]')) {
	// 	document.querySelector('.resources-page [js-hook="add-new-employee"]').addEventListener('click', (event) => {
	// 		var htmlString = '<div class="resource is-hidden" js-hook="resource"><div class="top"><div class="item"><img class="toggle" src="/assets/images/caret.svg" js-hook="toggle-resource"></div><div class="item form-group"><input class="form-control" type="text" placeholder="Employee name" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="Title/role" value=""></div><div class="item form-group compensation"><input class="form-control cost" type="text" placeholder="e.g., $30,000" value=""></div><div class="item action-items"><img class="delete-icon" js-hook="delete-resource" src="/assets/images/trash.svg"><img class="copy-icon" js-hook="copy-resource" copy-resource" src="/assets/images/copy.svg"><div class="allocation">0%</div></div></div><div class="bottom"><div class="form-group resource-description"><textarea class="form-control" id="question-input-5" placeholder="Please provide a short description of the employee\'s responsibilities" type="text"></textarea></div><div class="alert"><div class="icon"></div><div class="message">Some text about checking to make sure vacation or sick time is included in the allocated estimate.</div></div><div class="projects"><div class="headers"><div class="header">projects</div><div class="header">allocation</div></div><hr><div class="project-list"><div class="project"><div class="item"><a href="">Client Webapp</a></div><div class="item form-group"><input class="form-control cost" type="text" placeholder="e.g., $2,500" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="0%" value=""></div></div><div class="project"><div class="item"><a href="">Mobile Scanner with Laser Beams</a></div><div class="item form-group"><input class="form-control cost" type="text" placeholder="e.g., $2,500" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="0%" value=""></div></div></div></div></div></div>';
	// 		event.target.insertAdjacentHTML('beforebegin', htmlString);
	// 		setTimeout(() => {
	// 			document.querySelector('.resources-page [js-hook="payroll-tab"] .resources .resource.is-hidden').classList.remove("is-hidden");
	// 		}, 100);
	// 	});
	// }

	// // ADD NEW CONTRACTOR
	// if (document.querySelector('.resources-page [js-hook="add-new-contractor"]')) {
	// 	document.querySelector('.resources-page [js-hook="add-new-contractor"]').addEventListener('click', (event) => {
	// 		var htmlString = '<div class="resource is-hidden" js-hook="resource"><div class="top"><div class="item"><img class="toggle" src="/assets/images/caret.svg" js-hook="toggle-resource"></div><div class="item form-group"><input class="form-control" type="text" placeholder="Name" value=""></div><div class="item form-group compensation"><input class="form-control cost" type="text" placeholder="e.g., $30,000" value=""></div><div class="item action-items"><img class="delete-icon" js-hook="delete-resource" src="/assets/images/trash.svg"><img class="copy-icon" copy-resource" src="/assets/images/copy.svg"><div class="allocation">0%</div></div></div><div class="bottom"><div class="form-group resource-description"><textarea class="form-control" id="question-input-5" placeholder="Please provide a short description" type="text"></textarea></div><div class="alert"><div class="icon"></div><div class="message">Some text about checking to make sure vacation or sick time is included in the allocated estimate.</div></div><div class="projects"><div class="headers"><div class="header">projects</div><div class="header">allocation</div></div><hr><div class="project-list"><div class="project"><div class="item"><a href="">Client Webapp</a></div><div class="item form-group"><input class="form-control cost" type="text" placeholder="e.g., $2,500" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="0%" value=""></div></div><div class="project"><div class="item"><a href="">Mobile Scanner with Laser Beams</a></div><div class="item form-group"><input class="form-control cost" type="text" placeholder="e.g., $2,500" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="0%" value=""></div></div></div></div></div></div>';
	// 		event.target.insertAdjacentHTML('beforebegin', htmlString);
	// 		setTimeout(() => {
	// 			document.querySelector('.resources-page [js-hook="contracting-tab"] .resource.is-hidden').classList.remove("is-hidden");
	// 		}, 100);
	// 	});
	// }

	// // ADD NEW SUPPLY
	// if (document.querySelector('.resources-page [js-hook="add-new-supply"]')) {
	// 	document.querySelector('.resources-page [js-hook="add-new-supply"]').addEventListener('click', (event) => {
	// 		var htmlString = '<div class="resource is-hidden" js-hook="resource"><div class="top"><div class="item"><img class="toggle" src="/assets/images/caret.svg" js-hook="toggle-resource"></div><div class="item form-group"><input class="form-control" type="text" placeholder="Expense" value=""></div><div class="item form-group compensation"><input class="form-control cost" type="text" placeholder="e.g., $30,000" value=""></div><div class="item action-items"><img class="delete-icon" js-hook="delete-resource" src="/assets/images/trash.svg"><img class="copy-icon" copy-resource" src="/assets/images/copy.svg"><div class="allocation">0%</div></div></div><div class="bottom"><div class="alert"><div class="icon"></div><div class="message">Some text about checking to make sure vacation or sick time is included in the allocated estimate.</div></div><div class="projects"><div class="headers"><div class="header">projects</div><div class="header">allocation</div></div><hr><div class="project-list"><div class="project"><div class="item"><a href="">Client Webapp</a></div><div class="item form-group"><input class="form-control cost" type="text" placeholder="e.g., $2,500" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="0%" value=""></div></div><div class="project"><div class="item"><a href="">Mobile Scanner with Laser Beams</a></div><div class="item form-group"><input class="form-control cost" type="text" placeholder="e.g., $2,500" value=""></div><div class="item form-group"><input class="form-control" type="text" placeholder="0%" value=""></div></div></div></div></div></div>';
	// 		event.target.insertAdjacentHTML('beforebegin', htmlString);
	// 		setTimeout(() => {
	// 			document.querySelector('.resources-page [js-hook="supplies-tab"] .resources .resource.is-hidden').classList.remove("is-hidden");
	// 		}, 100);
	// 	});
	// }

	// // DELETE RESOURCE
	// if (document.querySelector('.resources-page [js-hook="delete-resource"]')) {
	// 	Array.from(document.querySelectorAll('.resources-page [js-hook="delete-resource"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="resource"]').classList.add("is-hidden");
	// 			setTimeout(() => {
	// 				event.target.closest('[js-hook="resource"]').parentNode.removeChild(event.target.closest('[js-hook="resource"]'));
	// 			}, 1000);
	// 		});
	// 	});
	// }

	// // COPY RESOURCE
	// if (document.querySelector('.resources-page [js-hook="copy-resource"]')) {
	// 	Array.from(document.querySelectorAll('.resources-page [js-hook="copy-resource"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.closest('[js-hook="resources"]').append(event.target.closest('[js-hook="resource"]').cloneNode(true), document.querySelector('.resources-page .resources').lastChild.previousSibling);
	// 			document.querySelector('.resources-page .resources').lastChild.previousSibling.classList.remove("is-open");
	// 			document.querySelector('.resources-page .resources').lastChild.previousSibling.classList.add("is-hidden");
	// 			setTimeout(() => {
	// 				document.querySelector('.resources-page .resources').lastChild.previousSibling.classList.remove("is-hidden");
	// 			}, 100);
	// 		});
	// 	});
	// }

	// // TRIGGER ALERT
	// if (document.querySelector('.resources-page [js-hook="input-percentage"]')) {
	// 	Array.from(document.querySelectorAll('.resources-page [js-hook="input-percentage"]')).forEach(function(element) {
	// 		element.addEventListener('blur', (event) => {
	// 			if (parseInt(event.target.value) > 100) {
	// 				event.target.closest('.bottom').querySelector('[js-hook="alert"]').classList.add("is-showing");
	// 				event.target.closest('.bottom').querySelector('[js-hook="alert"]').classList.add("warning");
	// 			}
	// 		});
	// 	});
	// }


	// // TOGGLE DETAILS ON DASHBOARD
	// if (document.querySelector('.dashboard-page [js-hook="details-toggle"]')) {
	// 	Array.from(document.querySelectorAll('.dashboard-page [js-hook="details-toggle"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			event.target.classList.toggle("is-open");
	// 		});
	// 	});
	// }

	// // TAB SELECTION
	// if (document.querySelector('[js-hook="chapter-selector"]')) {
	// 	Array.from(document.querySelectorAll('[js-hook="chapter-selector"]')).forEach(function(element) {
	// 		element.addEventListener('click', (event) => {
	// 			document.querySelector(".section-content .is-showing").classList.remove("is-showing");
	// 			document.querySelector(".chapter-nav .selected").classList.remove("selected");
	// 			document.querySelector("[js-hook=" + event.target.getAttribute("data-id") + "]").classList.add("is-showing");
	// 			event.target.classList.add("selected");
	// 		});
	// 	});
	// }

	// // OPEN ASK MONIKA
	// if (document.querySelector('.questionnaire-navigation [js-hook="ask-monika"]')) {
	// 	document.querySelector('.questionnaire-navigation [js-hook="ask-monika"]').addEventListener('click', (event) => {
	// 		event.target.parentNode.classList.add("is-open");
	// 	});
	// }

	// // OPEN SUBMIT FOR REVIEW
	// if (document.querySelector('.chapter.summary [js-hook="submit-review"]')) {
	// 	document.querySelector('.chapter.summary [js-hook="submit-review"]').addEventListener('click', (event) => {
	// 		event.target.classList.add("is-open");
	// 	});
	// }

	// // SUBMIT REVIEW
	// if (document.querySelector('.chapter.summary [js-hook="form-submit-review"]')) {
	// 	document.querySelector('.chapter.summary [js-hook="form-submit-review"]').addEventListener('submit', (event) => {
	// 		event.preventDefault();
	// 		document.querySelector('.chapter.summary [js-hook="form-submit-review"]').classList.add("is-submitting");
	// 		setTimeout(() => {
	// 			document.querySelector('.chapter.summary [js-hook="submit-review"]').classList.add("is-submitted");
	// 		}, 2000);
	// 	});
	// }

	// // SUBMIT ASK MONIKA
	// if (document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"]')) {
	// 	document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"]').addEventListener('submit', (event) => {
	// 		event.preventDefault();
	// 		if (document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"] textarea').value=== "") {
	// 			document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"] textarea').classList.add("shake");
	// 			setTimeout(() => {
	// 				document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"] textarea').classList.remove("shake");
	// 			}, 800);
	// 		} else {
	// 			document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"]').classList.add("is-submitting");

	// 			setTimeout(() => {
	// 				document.querySelector('.questionnaire-navigation [js-hook="form-ask-monika"]').parentNode.classList.add("is-submitted");
	// 			}, 2000);
	// 		}
	// 	});
	// }

});
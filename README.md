# Static Site Generator #

This is a static site generator. It allows for the generation of subpages using Markdown which can be indexed by google. It's hosted on Amazon's CDN.

## Setup ##
If you received any "permission error"s when you run a command in the terminal, preface the command with ```sudo ``` to run the command as an administrator.

### Git ###
Git is a version control system that allows multiple users to access and change a projects files without over-writing each other's changes by accident.

1. Download and install the latest [Git for Mac installer](https://sourceforge.net/projects/git-osx-installer/).
2. Open a terminal and verify the installation was successful by typing ```git --version```
3. Set your username with the following command. ```git config --global user.name "Your Username"```
4. Confirm that you have set your username correctly with the following command. ```git config --global user.name```
5. Clone the Git Repo (the project files) onto your computer by running the following command: ```git clone https://schonne@bitbucket.org/schonne/amp_boilerplate.git```. This will make a folder called "squirrel_static" and download all the project files into it. By default, this folder will be placed in your computer's user folder.
6. After the repo is downloaded, type ```cd squirrel_static``` to move the terminal into that directory.

### NPM ###
NPM is a part of Node.js, a programing framework. NPM and Node.js are used to build the static site on your computer.

1. Download and install the latest [Nodejs for Mac installer](https://nodejs.org/en/download/). 
2. Open a terminal and verify the installation was successful by typing ```npm -v```. It should give you a version number. If it doesn't, try step 1 again.
3. Install gulp (the build tool) by typing ```sudo npm install gulp -g``` in the terminal.
4. Make sure you're in the "squirrel_static directory" in your terminal. If you aren't sure and you saved the folder in the default location, close and reopen your terminal and type ```cd squirrel_static```
5. Type ```git clone https://schonne@bitbucket.org/schonne/tools.git```
6. Run ```npm install``` This will install all the files for node.js. This may take a while...
7. Type ```gulp``` in the terminal after the npm install finishes. This will build the site. You might get an error the first time you run gulp. If so, type ```gulp``` again and it should complete.
8. Run ```gulp server``` to see the built site
9. click control+c a couple times in the terminal to get it to stop acting like a server and allow you to use it again.

## Editing Content ##
Inside the squirrel_static/src/content/pages is the markdown files. Each file is a page on the site. Each md file has a section at the top that helps the build tool create the pages.

Slug = the subpage url "/pagename"  
Title = the page title that will be the H1 and meta-tag of the page. It should be optimized for SEO.  
Template = should always be "page.hbs"  

Below this top section, anything can be written using standard markdown.  

### Before you edit any content, run ```git pull``` to get the latest version of the site!!! ###

Once you're done adding/editing pages, you can build the site: 
 
1. Run ```gulp``` to build the site.  
2. Run ```gulp server``` to see the built site  
3. click control+c a couple times in the terminal to get it to stop acting like a server and allow you to use it again.  


## Uploading Site ##
*You must have a free [bitbucket](https://bitbucket.org/) account and be added to this repo to save changes to git.*

Once you're satisfied with the changes do the following:

1. Save the changes to git by typing ```git add -A``` (you may want to write down or screenshot the files that were added or changed after you run git add for step 3) followed by ```git commit -m 'description of changes you made'``` and finally ```git push```  
2. Now that the changes are saved, we can upload the site to AWS. Log into [Amazon S3](https://console.aws.amazon.com/s3/home?region=us-west-2#&bucket=squirreldm&prefix=)
3. There's no way to just overwrite everything like FTP with the S3 web uploader. You can only upload files, not folders. So you'll need to make sure the S3 matches your local folder structure. If you've added new pages, you'll need to create a folder in S3, click on it and put in the index.html file one at a time. Also overwrite any files that you changed so the latest version is on the S3. Having the list of changes from step 1 will be helpful here.
4. The changes you uploaded won't immediately appear on the squirreldigitalmarketing.com site. It can take about 15 min for the changes to be cached by the servers.
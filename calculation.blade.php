<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <style>
            header,
            footer {
                display: none;
            }

            .step,
            .info,
            .total {
                padding: 20px;
                margin-top: 20px;
                margin-bottom: 20px;
                border-radius: 5px;
                font-size: 16px;
            }

            .info > span,
            .total > span {
                display: inline-block;
                float: right;
            }

            .step {
                border: 2px solid #1a2d51;
            }

            .info {
                background: #f1f2f5;
            }

            .total {
                background: #4575c4;
                color: #fff;
            }

            .title-page strong,
            .total>strong {
                color: #fff;
            }

            .container {
                width: 650px;
                margin: 0 auto;
            }

            ol {
                list-style: none;
            }

            * {
                font-family: "Open Sans", sans-serif;
            }

            strong {
                color: #4575c4;
            }

            .title-page {
                height: 900px;
                width: 850px;
                background-color: #4d9634;
                text-align: center;
                margin: 0 auto;
                padding-top:300px;
                color:#fff;
            }

            .title-page h1 {
                margin:auto;
                font-size: 32px;
            }

            .company {
                background: #44813b;
                border-radius: 15px;
                display: inline-block;
                font-size: 24px;
                padding: 12px 100px;
                margin: 0 auto;
                width: 50%;
            }

            span {
                color: #7eb26e;
                font-weight: bold;
            }
            .summary {
                background-color: #4577c5;
                border-radius: 5px;
                text-align: center;
                color: #fff;
                padding:10px;
                margin-bottom: 20px;
            }
            .summary > p {
                font-size: 60px;
                margin: 10px auto;
/*                width: 100%;
                padding: 2px;
                margin: 2px;
*/
            }
            .subtotal {
                width: 50%;
                float: left;
                padding:10px;
                box-sizing: border-box;
            }
            .subtotal > div {
                background: #fff;
                border-radius: 10px;
                color: #4577c5;
                padding:10px;
                margin-top: 10px;
            }
            .clearfix:after {
                content: "";
                display: table;
                clear: both;
            }
            hr {
                margin: 20px 0;
                background-color: #fff;
                border-top: 2px dashed #001937;
            }

            .title-page hr {
                background-color: #4d9634;
            }

            .check {
                padding-top: 350px;
            }

            h2 {
                font-size: 23px;
                color: black;
                text-align: left;
                margin-top: 100px;
            }

            .pagebreak { page-break-before: always; }
        </style>
    </head>
    <body>
        <div class="title-page">
            <div class="container">
                <div style="margin-bottom: 40px;">
                    <h1>R&D Credit Calculations Report</h1>
                    <br/><br/>
                    <strong>PREPARED BY CLARUS R<span style="font-size: 23px">&#43;</span>D</strong>
                </div>
<!--                 <div style="margin-bottom: 80px;">
                    <strong>PREPARED BY CLARUS SOLUTIONS</strong>
                </div>
 -->                <div class="company">{{$company->name}}</div>
                <div class="check">
                    <p id="date">{{$date}}</p>
                    <hr>
                    1233 Dublin, Columbus, OH 43215&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    614.545.9100
                </div>
            </div>
        </div>
        <div class="pagebreak"></div>
        <div class="container">
            <ol>
                <li>
                    <h1>Tax Credit Summary</h1>
                    <div class="summary clearfix">
                        Total Estimated Tax Credit:
                        <p>{{$data['total_credit']}}</p>
                    </div>

                    {{-- This is where you want to loop through each project --}}

                    <hr>
                    <h2>{{$project->name}}</h2>
                    <strong>Internal Payroll Expense</strong>
                    <div class="info">
                        Total
                        <span>{{$calculations['payroll']}}</span>
                    </div>
                    <div class="info">
                        Qualified %
                        <span>100%</span>
                    </div>
                    <div class="info">
                        Qualified Amount
                        <span>{{$calculations['payroll']}}</span>
                    </div>
                    <br/>
                    <strong>Third Party Contracting Expense</strong>
                    <div class="info">
                        Total
                        <span>{{$calculations['contractingTotal']}}</span>
                    </div>
                    <div class="info">
                        Qualified %
                        <span>65%</span>
                    </div>
                    <div class="info">
                        Qualified Amount
                        <span>{{$calculations['contracting']}}</span>
                    </div>
                </li>
                <hr>
                <div class="pagebreak"></div>
                <li>
                    <strong>Other Qualifying Expenses</strong>
                    <div class="info">
                        Total
                        <span>{{$calculations['supplies']}}</span>
                    </div>
                    <div class="info">
                        Qualified %
                        <span>100%</span>
                    </div>
                    <div class="info">
                        Qualified Amount
                        <span>{{$calculations['supplies']}}</span>
                    </div>
                </li>                
                {{-- This is where you want to loop through each project's expenses --}}
                <div class="pagebreak"></div>
                <li>
                    <hr>
                    <h1>{{$project->name}}</h1>
                    <h2>Internal Payroll Expense</h2>
                    @if(empty($expenses[1]))
                        <div>
                            <div class="info">
                                Compensation
                                <span> </span>
                            </div>
                            <div class="info">
                                Allocation %
                                <span> </span>
                            </div>
                        </div>
                    @else
                        @foreach($expenses[1] as $expense)
                            <div>
                                <table style="margin-top:50px;">
                                    <tr>
                                        <td>Employee Name:</td>
                                        <td><strong>{{$expense->name}}</strong></td>
                                    </tr>
                                    <tr>
                                        <td>Title/Role:</td>
                                        <td><strong>{{$expense->title}}</strong></td>
                                    </tr>
                                </table>
                            </div>
                            <div class="info">
                                Compensation
                                <span>{{$expense->amount}}</span>
                            </div>
                            <div class="info">
                                Allocation %
                                <span>{{$expense->allocation_percent . '%'}}</span>
                            </div>
                            @if ($expense->allocation_percent>=80)
                            <div>*This allocation is greater then or equal to 80%, therefore substantially all time spent on qualifying
                                activities and 100% of wages will be used for the credit calculation.
                                Reg. 1.41-2(d)(2)</div>
                            @endif
                            <div>
                                Duties and responsibilities:
                                <p><strong><i>{{$expense->duties}}</i></strong></p>
                            </div>
                        @endforeach
                    @endif
                </li>
                <hr>
                <div class="pagebreak"></div>
                <li>
                    <h1>Third Party Contracting Expense</h1>
                    @if(empty($expenses[2]))
                        <div>
                            <div class="info">
                                Compensation
                                <span> </span>
                            </div>
                            <div class="info">
                                Allocation %
                                <span> </span>
                            </div>
                        </div>
                    @else
                        @foreach($expenses[2] as $expense)
                            <div>
                                <table style="margin-top:50px;">
                                    <tr>
                                        <td>Contractor Name/Organization:</td>
                                        <td><strong>{{$expense->name}}</strong></td>
                                    </tr>
                                </table>
                                <div class="info">
                                    Compensation
                                    <span>{{$expense->amount}}</span>
                                </div>
                                <div class="info">
                                    Allocation %
                                    <span>{{$expense->allocation_percent . '%'}}</span>
                                </div>
                                <div>
                                    Duties and responsibilities:
                                    <p><strong><i>{{$expense->duties}}</i></strong></p>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </li>
                <hr>
                <div class="pagebreak"></div>
                <li>
                    <h1>Other Qualifying Expense</h1>
                    @if(empty($expenses[3]))
                        <div>
                            <div class="info">
                                Compensation
                                <span> </span>
                            </div>
                            <div class="info">
                                Allocation %
                                <span> </span>
                            </div>
                        </div>
                    @else
                        @foreach($expenses[3] as $expense)
                            <div>
                                <table style="margin-top:50px;">
                                    <tr>
                                        <td>Expense Type:</td>
                                        <td><strong>{{$expense->name}}</strong></td>
                                    </tr>
                                </table>
                                <div class="info">
                                    Amount
                                    <span>{{$expense->amount}}</span>
                                </div>
                                <div class="info">
                                    Allocation %
                                    <span>{{$expense->allocation_percent . '%'}}</span>
                                </div>
                            </div>
                        @endforeach
                    @endif
                </li>
                <hr>
            </ol>
        </div>
    </body>
</html>
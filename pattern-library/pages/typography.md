# Typography

## Sofia Pro

##### Font stack
For both mobile and web properties, the font stack should specify `sofia-pro, "Helvetica Neue", Helvetica, Arial, sans-serif`. This font is primarily used for headings.

##### Glyphs
<div style="font-size: 32px; line-height: 1.4; font-family: sofia-pro, 'Helvetica Neue', Helvetica, Arial, sans-serif;">
ABCDEFGHIJKLMNOPQRSŠTUVWXYZŽ
<br/>
abcdefghijklmnopqrsštuvwxyzž
<br/>
ÂÊÔâêô
<br/>
1234567890
<br/>
‘?’“!”(%)[#]{@}/&\<-+÷×=>®©$€£¥¢:;,.*</div>

##### Headers
# Header 1: Experienced Claims and Risk Management Specialists
## Header 2: Experienced Claims and Risk Management Specialists
### Header 3: Experienced Claims and Risk Management Specialists 
#### Header 4: Experienced Claims and Risk Management Specialists
##### Header 5: Experienced Claims and Risk Management Specialists
<hr/>

## Source Sans Pro

##### Paragraph
I will be the leader of a company that ends up being worth billions of dollars, because I got the answers. I understand culture. I am the nucleus. I think that’s a responsibility that I have, to push possibilities, to show people, this is the level that things could be at.


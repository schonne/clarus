# Colors

## Usage
Clarus R+D's app color palette comprises primary and accent colors that can be used for primary and secondary actions. They’ve been designed to work harmoniously with each other. 


## App Chrome
The primary color scheme of Clarus R+D apps are blue-grey chrome elements on a white background. These muted colors keeps the user's focus on the content and makes room for using color to highlight important information and push the user towards primary actions


<div class="color-tag light" style="background-color: #1571D4;">
<span class="group" style="font-size: 22px;margin-bottom: 60px;">Denim</span>
<div class="details">
<span class="hex" style="font-size: 22px;">#1571D4</span>
</div>
</div>
<div class="color-tag light" style="background-color: #ffffff;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 60px; color: #2b2a28">White</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#ffffff</span>
</div>
</div>
<br/><br/>
<div class="color-tag light" style="background-color: #092F57;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 25px; color: #ffffff">Madison</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #ffffff">#092F57</span>
</div>
</div>
<div class="color-tag light" style="background-color: #F2F3F7;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 25px; color: #2b2a28">Athens Gray</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#F2F3F7</span>
</div>
</div>
<div class="color-tag light" style="background-color: #F7F7F7;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 25px; color: #2b2a28">Alabaster</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#F7F7F7</span>
</div>
</div>
<div class="color-tag light" style="background-color: #E5E5E5;border: solid #ecf1f3 1px;">
<span class="group" style="font-size: 22px;margin-bottom: 25px; color: #2b2a28">Mercury</span>
<div class="details">
<span class="hex" style="font-size: 22px; color: #2b2a28">#E5E5E5</span>
</div>
</div>

## Primary Actions / Highlight
Use Christi to bring the user's attention to something important in the UI, or to point them towards an action you'd like them to take.

<div class="color-tag light" style="background-color: #55920C;">
<span class="group" style="font-size: 22px;margin-bottom: 60px;">Christi</span>
<div class="details">
<span class="hex" style="font-size: 22px;">#55920C</span>
</div>
</div>
<div class="color-tag light" style="background-color: #D60404;">
<span class="group" style="font-size: 22px;margin-bottom: 60px;">Monza</span>
<div class="details">
<span class="hex" style="font-size: 22px;">#D60404</span>
</div>
</div>

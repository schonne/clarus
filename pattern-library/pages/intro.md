# Introduction

__**The latest version of the Clarus R+D style sheets, fonts and icons can be found**__ [here](/ClarusStyleGuide.zip)

This Clarus R+D Software Style Kit should be referenced when building all applications within Clarus R+D. It's important that each software project we produce has some consistent design principles and patterns.

This document lays out the baseline design standards for all Clarus R+D software. It can be should be referenced throughout the development of your project.

Clarus R+D's Style Guide will help you create a clean and simple web project that is a perfect fit for today's flat design. It makes use of bold colours, beautiful typography, clear photography and spacious arrangements.

# Layout


All Clarus R+D software should follow a basic structure. This keeps the experience the same for the users across different projects. Having a consistent design flow across projects also makes it easier for a user to get up and running.

The layout of Clarus R+D software has a few basic elements:

![alt text](app/images/basiclayout.jpg "Logo Title Text 1")

![alt text](app/images/basiclayoutdefinitions.jpg "Logo Title Text 1")

## Global Navigation
__View:__ Always visible - fixed to top 

The Global Navigation is a global element and house both the application logo and the Global Navigation links. It should be accessible throughout the application. Generally these are parts of the app specific to the user as opposed to sections specific to the function of the app.

Some examples of common Global Navigation links are:

- Logout
- Settings
- User Profle
- Help
- Notifications
- Messages
- Todos

## Questionnaire Navigation
__View:__ Found on most pages

The Questionnaire Navigation is your appication's main navigation. Major categories should appear here. 

## Primary View
__View:__ Shows the page's primary view, the one that solves the user's primary problem on this view

This section is where the primary page experience lives.


## Help Card
__View:__ Gives prominent real estate to facilitate assistance.

This section is optional but should be used when dealing with non-specialist personas.

## Footer Navigation
__View:__ Shows legal information about the app

This area will store any links to terms or privacy policies.
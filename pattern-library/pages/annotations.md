# Annotations

## Usage
Clarus R+D's app allows for commenting inline throught the customer experience. This allows feedback and support in areas of concern in context of the issue.


## Icon
Annotation icons appear on the right side margin of the forms. Clicking on the icon will open a popover allowing the user to view or add comments at that location.

![alt text](app/images/annotation1.jpg "Annotations")

### Add New Comment
![alt text](app/images/annotation2.jpg "Annotations")

This icon is hidden unless the user hovers over the section which will reveal the icon.

Clicking on the icon will reveal an empty popover allowing a new comment to be added.
![alt text](app/images/annotation3.jpg "Annotations")

### Unread Comment
![alt text](app/images/annotation4.jpg "Annotations")

This icon appears if there is an unread message in the annotation. Opening the popover will display the comments.

### Read Comment
![alt text](app/images/annotation6.jpg "Annotations")

This icon appears if there is a read message in the annotation. Opening the popover will display the comments.

### Resolved Comment
![alt text](app/images/annotation7.jpg "Annotations")

This icon appears if the comment has been marked "resolved". Opening the popover will display the comments.

## Comment Popover
![alt text](app/images/annotation5.jpg "Annotations")

The comment popover appears when clicking on an annotation icon. The user can click the icon again to close it.

### Scrolling
The comment popover should have a maxium height (200px is suggested) and is scrollable inside to allow older messages to be viewed. However the action items at the bottom should be fixed so they are always available for the user to interacti with, even if they have scrolled up to older messages.


### Message Order
Messages should be ordered with the newest one at the bottom, making it easier for the user to reply directly to that message.

### Action items
At the bottom of the popover are a few action items.

#### Leave a comment
Clicking this will initate a new comment at the bottom of the list for the user to create.

![alt text](app/images/annotation8.jpg "Annotations")


#### Mark as resolved
Clicking this will set the annotations status to "resolved" which means it no longer requires any further action

![alt text](app/images/annotation9.jpg "Annotations")

#### Interal only
This keeps the comment the user created visible to only admins
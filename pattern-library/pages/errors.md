# Errors
#### Errors occur when an app fails to complete an expected action.

## Usage
Errors occur when an app fails to complete an action, such as:

- The app does not understand user input
- The system or app fails
- A user intends to run incompatible operations concurrently

Minimize errors by designing apps that make it easy for users to input information flexibly. Apps should accept common data formats that use affordances to improve user understanding.

To address errors:

- Clearly communicate what is happening
- Describe how a user can resolve it
- Preserve as much user-entered input as possible

## User input errors
Help users fix input errors as soon as they are detected. Disable the submission of a form if errors are detected, and if detected only after form submission, clearly explain the error and how to fix it.

### Text field input
*Helper text* may be included before, during, or after a user interacts with each field on a form.

![alt text](app/images/error.jpg "Error Message")

Show *error text* only after user interaction with a field. If the user inputs incorrect data, helper text may transform into error text.

Minimize form text to the essentials. Not every text field needs helper and/or error text.

### Form submission
The Submit button should be enabled by default.

If you are performing inline form validation, and the field with the error is clearly marked, the submit button may be disabled until the error is corrected.

### Color
Error text should be #D60404, which is the error color used in the Clarus R+D Style Kit.

Error text should be legible, with noticeable contrast against its background color.


### Incompatible values
Show errors for incompatible values during or after a user interacts with a text field.

If two or more fields have incompatible inputs:

- In the text field, indicate a fix is needed. Add an error message below.
- Display a message at the top of the form, or screen, summarizing the fixes needed and any additional explanation

### Errors detected upon form submission
Reload the form with consolidated error messages and scroll position at the top. Error messages for individual fields may be resolved as the user works through the form.

### Incomplete form
Empty form fields should be indicated by both the text field and error message below.

Display incomplete form errors to indicate a user has skipped a field after they have advanced through a form. If unable to detect user progress through the form, display an error after the user has attempted to submit the form.

### Multiple errors before form submission
Individually label error messages as the user works through the form.


## App errors
App errors occur independent of user input.

### General app error
When an error occurs, an app should display loading indicators until the error message appears.

Features not available may be indicated as disabled in the UI. For example, a button not may be displayed in a disabled state, paired with text explaining it is not available. Not every error requires a new component to pop up.

If possible, give your user an action that will help them address the error.

### Sync error/failure to load
When sync is down or content has failed to load, the user should be able to interact with as much of the rest of the app as possible.

### Connectivity
When connectivity is down, the user should be able to interact with as much of the rest of the app as possible.

If appropriate, present a link to help a user accomplish their task. Only offer links that you can actually support. For example, don't offer an option like "Try again" in cases where you can detect that the operation will fail.

## Incompatible state errors
Incompatible state errors occur when users attempt to run operations that conflict, such as making a call while in airplane mode or taking a screenshot from a restricted work account. Help prevent users from putting themselves into these situations by clearly communicating the states they are selecting and the implications for the rest of their experience. When these errors are triggered, do not imply that they are the user’s fault.

### General incompatibility
Clarify the reason for and origination of the error.

### Permission requested
If your app requires user permission before proceeding with an action, include the permission request in the app flow instead of treating it as an error.

If permissions are necessary before the first run of an app, consider including them into your app’s first-run experience.

Examples:

- An app’s permissions have changed
- In-app purchases have been disabled
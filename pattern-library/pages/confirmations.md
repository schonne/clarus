# Confirmation & acknowledgement
#### When a user invokes an action in your app, confirm or acknowledge that action through text.

![alt text](app/images/confirmation.jpg "Error Message")

## Usage
Confirming and acknowledging actions can help alleviate uncertainty about things that have happened or will happen. It also prevents users from making mistakes they might regret.

- Confirming involves asking the user to verify that they want to proceed with an action
- Acknowledging is displaying text to let the user know that the action they chose has been completed
Not all actions warrant a confirmation or an acknowledgment.




## Confirmation
A confirmation asks the user to verify that they truly want to proceed with the action they just invoked. It may be paired with a warning or critical information related to that action.

Dialog titles for confirmations should be meaningful and echo the requested action.

Confirmation isn’t necessary when the consequences of an action are reversible or negligible. For example, if a check mark shows an image has been selected, further confirmation is unnecessary.

Confirmation dialogs should appear in context with the action performed. Modals aren't ever used.




## Acknowledgement
Acknowledgement removes uncertainty about implicit operations that the system is taking. It may be paired with an option to undo the action.


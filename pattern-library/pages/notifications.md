# Notifications
#### Notifications provide short, timely, and relevant information from your app

## Usage
Notifications are intended to inform users about events in your app. These two types of notifications are the most effective:

- Communication from other users
- Well-timed and informative task reminders

## When not to use a notification
Notifications should not be the primary communication channel with your users, as frequent interruptions may cause irritation. The following cases do not warrant notification:

- Operations that don't require user involvement, like syncing information
- Error states from which the app may recover without user interaction

<html lang="{{ app()->getLocale() }}">
    <head>
        <meta charset="utf-8">
        <style>
            header,
            footer {
                display: none;
            }

            .step,
            .info,
            .total {
                padding: 20px;
                margin-bottom: 20px;
                border-radius: 5px;
                font-size: 16px;
            }

            .info > span,
            .total > span {
                float: right;
                font-weight: bold;
                display: inline-block;
            }

            .info > span {
                color: #4d9634;
            }

            .step {
                border: 2px solid #1a2d51;
            }

            .info {
                background: #f1f2f5;
            }

            .total {
                background: #4575c4;
                color: #fff;
            }

            .title-page strong,
            .total>strong {
                color: #fff;
            }

            .container {
                width: 640px;
                margin: 0 auto;
            }

            ol {
                list-style: none;
            }

            * {
                font-family: "Open Sans", sans-serif;
            }

            h1,
            h2,
            h3,
            h4,
            h5,
            h6 {
                margin-left: 45px;
            }

            strong {
                color: #4575c4;
            }

            .title-page {
                height: 900px;
                width: 850px;
                background-color: #1a2d51;
                text-align: center;
                margin: 0 auto;
                padding-top:300px;
                color:#fff;
            }

            .title-page h1 {
                margin:auto;
                font-size: 32px;
            }

            .company {
                padding: 12px 100px;
                background: #001937;
                border-radius: 15px;
                display: inline-block;
                margin: 0 auto;
                font-size: 22px;
                width: 50%;
                overflow: auto;
            }

            hr {
                margin: 20px 0;
                background-color: #fff;
                border-top: 2px dashed #001937;
            }

            .title-page hr {
                background-color: #1a2d51;
            }

            .check {
                padding-top: 350px;
            }


            .pagebreak { page-break-before: always; }
        </style>
    </head>
    <body>
        <div class="title-page">
            <div class="container">
                <div style="margin-bottom: 40px;">
                    <h1>
                        R&D Credit, How This<br/> Was Calculated
                    </h1>
                    <br/><br/><br/><br/><br/>
                    <strong>PREPARED BY CLARUS R<span style="font-size: 23px">&#43;</span>D</strong>
                </div>

                <div class="company">
                    {{$company->name}}
                </div>
                <div  class="check">
                    <i style="font-style: normal;" id="date">{{$date}}</i>
                    <hr>
                    1233 Dublin, Columbus, OH 43215&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                    614.545.9100
                </div>
            </div>
        </div>
        <div class="pagebreak"></div>
        <div class="container">
            <h1>How was your credit calculated?</h1>
            <ol>
                <li>
                    @if ($data['type'] === 'regular')
                        <div class="step">
                            1. First, we calculate your <strong>Base Amount</strong>, which is greater of:
                            <br/>
                            <strong>50%</strong> of Qualified Expenses; or,<br />
                            <strong>3%</strong> of your average revenue for the four years preceding the credit year.
                        </div>
                        <br />
                        <p>Your Information</p>
                        <div class="info">
                            Your Average Revenue for the Previous 4 Years:
                            <span>{{$data['four_year_average']}}</span>
                        </div>
                        <div class="info">
                            3% of Your Average Revenue for Previous 4 Years:
                            <span>{{$data['three_percent_of_average']}}</span>
                        </div>
                        <br />
                        <div class="info">
                            Total Qualifying Expenses:
                            <span>{{$data['qre']}}</span>
                        </div>
                        <div class="info">
                            <strong>50%</strong> of Your Qualifying Expenses:
                            <span>{{$data['half_qre']}}</span>
                        </div>
                        <br />
                        <div class="info">
                            <strong>Your Base Amount:</strong>
                            <span>{{$data['base_amount']}}</span>
                        </div>
                    @else
                        <div class="step">
                            1. First, we calculate your <strong>Base Amount</strong> of your Average Qualified Expenses
                            for the prior 3 tax years, or zero if no qualifying expense in one of the three prior tax years.
                        </div>
                        <br />
                        <p>Your Information</p>
                        <div class="info">
                            Your Qualified Research Expenses for the Prior 3 Tax Years:
                            <span>{{$data['three_year_qre']}}</span>
                        </div>
                        <div class="info">
                            Your Average Qualified Research Expenses<br/>for the Prior 3 Tax Years:
                            <span>{{$data['three_year_average']}}</span>
                        </div>
                        <div class="info">
                            Your Base Amount:
                            <span>{{$data['base_amount']}}</span>
                        </div>
                    @endif
                </li>
                <hr>
                <div class="pagebreak"></div>
                <li>
                    @if ($data['type'] === 'regular')
                        <div class="step">
                            2. Second, we calculate your Total Credit, which is <strong>20%</strong> of qualifying expenses in excess of your base amount.
                        </div>
                        <br />
                        <p>Your Information</p>
                        <div class="info">
                            Your Base Amount:
                            <span>{{$data['base_amount_in_excess']}}</span>
                        </div>
                        <div class="info">
                            <strong>20%</strong> of your qualifying expenses in excess of base amount:
                            <span>{{$data['total_credit']}}</span>
                        </div>
                        <br />
                        <div class="total">
                            <strong>Your Total Credit:</strong>
                            <span>{{$data['total_credit']}}</span>
                        </div>
                    @else
                        <div class="step">
                            2. Second, we calculate your Total Credit, which is <strong>{{$data['multiplier'] * 100}}%</strong> of
                            qualifying expenses in excess of your base amount.
                        </div>
                        <br />
                        <p>Your Information</p>
                        <div class="info">
                            Total Qualifying Expenses:
                            <span>{{$data['qre']}}</span>
                        </div>
                        <div class="info">
                            Your Base Amount:
                            <span>{{$data['base_amount']}}</span>
                        </div>
                        <div class="info">
                            <strong>{{$data['multiplier'] * 100}}%</strong> of your qualifying expenses in excess of base amount:
                            <span>{{$data['total_credit']}}</span>
                        </div>
                        <br />
                        <div class="total">
                            <strong>Your Total Credit:</strong>
                            <span>{{$data['total_credit']}}</span>
                        </div>
                    @endif
                </li>
                <hr>
            </ol>
        </div>
    </body>
</html>